from __future__ import print_function
import numpy as np
import bisect


class CAGECluster(object):
    def __init__(self, o, bedcols):
        self.chrom = o[0]
        self.start = o[1]
        self.end = o[2]
        self.name = o[3]
        self.score = o[4]
        self.strand = o[5]
        self.data = o
        self.bedcols = bedcols

    def getCorrelation(self, other):
        '''Obtain the correlation between two clusters.
        '''
        names1 = self.data.dtype.names[self.bedcols:]
        names2 = other.data.dtype.names[other.bedcols:]
        if len(names1) != len(names2):
            print("clusters expression profile does not match.")
            return None

        names1 = sorted(names1)
        names2 = sorted(names2)
        for a, b in zip(names1, names2):
            if a != b:
                print("mismatched cell/tissue type")
                return None

        x = self.getExprs(names1)
        y = other.getExprs(names2)
        from scipy.stats import pearsonr
        return pearsonr(x, y)

    def getFoldChangeAgainstPercentile(self, targetName, percentile=50):
        '''
        Obtain the fold change against a specified percentile.
        params: targetName, the name of the column of interest.
        percentile: the percentile to use, default=50
        '''

        return np.log2(self.data[targetName] * 1.0 / (self.getExprAtPercentile(percentile) + 1))


    def getExprAtPercentile(self, percentile):
        names = self.data.dtype.names[self.bedcols:]
        x = self.getExprs(names)
        return np.percentile(x, percentile)

    def getExprs(self, names):
        x = []
        for i in names:
            x.append(self.data[i])
        return x

    def isEnhancer(self):
        return self.strand == '.'

    def asBed(self):
        return "\t".join(map(str, [self.chrom, self.start, self.end, self.name, self.score, self.strand]))


def loadCAGEClusters(filename, bedcols, clusters, thresh, expCol):
    f = np.genfromtxt(filename, delimiter="\t", dtype=None, names=True)
    for r in f:
        if r[expCol] < thresh:
            continue
        if r['chrom'] not in clusters:
            clusters[r['chrom']] = []
        clusters[r['chrom']].append(CAGECluster(r, bedcols))
    for c in clusters:
        clusters[c].sort(key=lambda k: (k.start, k.end))


def mapCAGEToRegions(regions, clusters, dist_allowed=0, sameStrand=False, enhancerOnly=False, tssOnly=False):
    for c in regions:
        if c not in clusters:
            continue

        cstarts = [cluster.start for cluster in clusters[c]]
        cends = [cluster.end for cluster in clusters[c]]
        for e in regions[c]:
            startIdx = bisect.bisect_left(cstarts, e.start-dist_allowed )
            #endIdx = bisect.bisect_right(cstarts, e.end)
            for cluster in clusters[c][startIdx:]:
                if cluster.start >= e.end + dist_allowed:
                    break
                if min(e.end, cluster.end) - max(e.start, cluster.start) > -dist_allowed:
                    if (not enhancerOnly and not tssOnly) or (tssOnly and not cluster.isEnhancer()) or (enhancerOnly and cluster.isEnhancer):
                        if not sameStrand or cluster.strand == "." or e.strand == "." or cluster.strand == e.strand:
                            e.addCluster(cluster)


def getClusterPairs(enhancers):
    pairs = []
    for c in enhancers:
        for e in enhancers[c]:
            partners = e.getPartners()
            for p in partners:
                for x in e.getClusters():
                    for y in p.getClusters():
                        if ((x.chrom < y.chrom) or (x.chrom == y.chrom and x.start < y.start) or
                                (x.chrom == y.chrom and x.start == y.start and x.end < y.end)):
                            pairs.append([x, y, e.getTypeStr(), p.getTypeStr()])
                        else:
                            pairs.append([y, x, p.getTypeStr(), e.getTypeStr()])
    pairs.sort(key=lambda k: (k[0].name, k[1].name))
    lastName1 = pairs[0][0].name
    lastName2 = pairs[0][1].name
    p = pairs[0]
    p += list(p[0].getCorrelation(p[1]))
    pairs_noDup = [p, ]
    for p in pairs[1:]:
        if p[0].name == lastName1 and p[1].name == lastName2:
            pass
        else:
            p += list(p[0].getCorrelation(p[1]))
            pairs_noDup.append(p)
            lastName1 = p[0].name
            lastName2 = p[1].name
    return pairs_noDup


def writeCAGEPairs(pairs, outFile):
    out = open(outFile, 'w')
    for p in pairs:
        out.write("\t".join([p[0].asBed(), p[1].asBed(), ] + map(str, p[2:])) + "\n")

    out.close()
