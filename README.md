### Sample usage

```
python Sein.py k562 -t cells --ogfile <PATH-TO-REPO>/data/og.txt --tsgfile <PATH-TO-REPO>/data/tsg.txt --ccgfile <PATH-TO-REPO>/data/ccg.txt --workingDir <absolute-path-to-working-directory>
```
Absolute paths should be specified for the input files. The output will be written in the working directory. Use `python Sein.py --help` to get more information. Many of the inputs are hardcoded in `Sein.py`.

### Dependencies:
 * python 2.7
 * numpy
 * scipy
 * matplotlib
 * prettyplotlib
 * statsmodels
 * graphviz (optional)

### Enhancer ranking plot
```
python enhancer_signal_by_rank.py
```

#### The following tex packages are required:
  * dvipng
  * texlive-latex-extra
  * texlive-fonts-recommended
  * texlive-science
