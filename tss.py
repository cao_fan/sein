from common import *

class TSS:
    def __init__(self, chrom, start, end, name, score, strand):
        self.chrom = chrom
        self.start = int(start)
        self.end = int(end)
        self.name = name
        self.score = int(score)
        self.strand = strand
        self.geneNames = []
        self.geneIds = []

        self.clusters = []

    def addCluster(self, cluster):
        self.clusters.append(cluster)

    def asBedStr(self):
        return "\t".join(map(str, [self.chrom, self.start, self.end, self.name, self.score, self.strand]))

    def getClusters(self):
        return self.clusters

    def setGeneNames(self, id2NameMap):
        def findNameForId(currId):
            if currId in id2NameMap:
                return id2NameMap[currId]
            else:
                return currId
        self.geneNames = map(lambda x: findNameForId(x), self.name.split(';'))

    def getNameStr(self, ogs=None, tsgs=None, hkgs=None, ccgs=None):
        ids = self.name.split(';')
        if len(self.geneNames) > 0:
            assert len(self.geneNames) == len(ids)
            if ogs is not None or tsgs is not None:
                nameStrs = []

                for a, b in zip(self.name.split(";"), self.geneNames):
                    tempStr = ""
                    if ogs is not None and b in ogs:
                        tempStr += "<OG>"
                    if tsgs is not None and b in tsgs:
                        tempStr += "<TSG>"
                    if hkgs is not None and b in hkgs:
                        tempStr +="<HKG>"
                    if ccgs is not None and b in ccgs:
                        tempStr +="<CCG>"

                    nameStrs.append("{}({}{})".format(a, b, tempStr))
                return ";".join(nameStrs)
            else:
                return ";".join(map(lambda (x,y): x+"({})".format(y), zip(ids, self.geneNames)))
        else:
            return self.name


    def get_max_cage_score(self, expCol, expThresh):
        max_score = 0
        for c in self.clusters:
            if c.getExprs([expCol])[0] >= expThresh and c.score > max_score:
                max_score = c.score
        return max_score


    def get_expression(self, column):
        total_expr = 0
        for c in self.clusters:
            total_expr += c.getExprs([column])[0]
        return total_expr



    def get_rnaseq_expression(self, rnaseq_expressions):
        result = []
        tss_names = self.name.split(';')
        for t in tss_names:
            if t in rnaseq_expressions:
                result.append(rnaseq_expressions[t])
            else:
                result.append(0)
        return result

    def get_rnaseq_gene_expression_for_og_tsg(self, rnaseq_gene_expression, ogs=None, tsgs=None, hkgs=None, ccgs=None, thresh=1):
        og_values = set()
        tsg_values = set()
        hkg_values = set()
        ccg_values = set()
        all_values = set()
        ids = self.name.split(';')
        if len(self.geneNames) > 0:
            assert len(self.geneNames) == len(ids)
            for a,b in zip(ids, self.geneNames):
                expr = rnaseq_gene_expression[a] if ((rnaseq_gene_expression is not None) and (a in rnaseq_gene_expression)) else 0
                if expr > thresh:
                    if ogs is not None and b in ogs:
                        og_values.add((b, expr))
                    if tsgs is not None and b in tsgs:
                        tsg_values.add((b, expr))
                    if hkgs is not None and b in hkgs:
                        hkg_values.add((b, expr))
                    if ccgs is not None and b in ccgs:
                        ccg_values.add((b, expr))
                    all_values.add((b, expr))

        return list(og_values), list(tsg_values), list(hkg_values), list(ccg_values), list(all_values)



def loadTssFromBed(filename, id2NameMap=None):
    result = {}
    f = open(filename)
    for r in f:
        tokens = r.strip().split()
        if tokens[0] not in result:
            result[tokens[0]] = []
        for i in [1,2,4]:
            tokens[i] = int(tokens[i])
        currTss = TSS(*tokens[:6])
        if id2NameMap != None:
            currTss.setGeneNames(id2NameMap)
        result[tokens[0]].append(currTss)

    for c in result:
        result[c].sort(key=lambda k:(k.start,k.end))

    f.close()
    return result

def __overlap(s1, e1, s2, e2):
    return max(s1,s2) - min(e1,e2) < 0

import bisect
def findNearestTss(chrom, start, end, qTsses, strand=".", overlap=False, center_dist = 0):
    '''
    The bisect algorithm assumes that all the intervals are sorted
    '''
    if chrom not in qTsses:
        return None
    startIdx = bisect.bisect_left([t.start for t in qTsses[chrom]],max(0, start-center_dist))

    if not overlap:
        retValues = []
        minDist = 10000000000
        for t in qTsses[chrom]:
            if not __overlap(t.start, t.end, start, end):
                tempDist = start - t.end if start-t.end >= 0 else t.start - end
                if tempDist < minDist:
                    retValues = [t,]
                    minDist = tempDist
                elif tempDist == minDist:
                    retValues.append(t)
        return retValues
    else:
        retValues = []
        for t in qTsses[chrom][startIdx:]:
            if __overlap(t.start, t.end, start, end) or abs((start+end-1)/2 - (t.start+t.end-1)/2) <= center_dist:
                retValues.append(t)
            elif (t.start+t.end-1)/2 - (start+end-1)/2 > center_dist:
                break
        return retValues

    return []

def mapTssToEnhancer(regions, tsses, center_dist = 0):
    for c in regions:
        for r in regions[c]:
            tempTsses = findNearestTss(r.chrom, r.start, r.end, tsses, overlap=True, center_dist=center_dist)
            r.addTsses(tempTsses)

def makeNameMapDict(filename, cols=(0,1), gzip=False, header=False):
    '''
    produce a map from IDs to gene names.
    cols is a tuple of two with the first indicating the column
    of the ID and the second indicating the column of the gene name.
    '''
    assert len(cols) == 2
    f = open(filename)
    if header:
        f.readline()
    mapDict = {}
    for r in f:
        tokens = r.strip().split()
        mapDict[tokens[cols[0]]] = tokens[cols[1]]
    f.close()
    return mapDict
