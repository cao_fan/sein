TYPE_SE = 8
TYPE_SP = 4
TYPE_TE = 2
TYPE_TP = 1
SCORE_SE = 8
SCORE_SP = 4
SCORE_TE = 2
SCORE_TP = 1

TYPES = [ TYPE_SE, TYPE_SP, TYPE_TE, TYPE_TP ]
TYPE_STRS = [ "DSE", "PSE", "DTE", "PTE" ]
BD_STRS = ["DBD", "PBD", "DTD", "PTD"]
BD_NAMES = ["Distal BD", "Proximal BD", "Distal TD", "Proximal TD"]
TYPE_NAMES = ["Distal SE", "Proximal SE", "Distal TE", "Proximal TE"]
INTRA_LABELS = ["Intra DSE", "Intra PSE", "Intra DTE", "Intra PTE"]
INTRA_COLORS = {
"PSE": "#034f84",
"PTE": "#92a8d1",
"DSE": "#9D2932",
"DTE": "#eea29a",
"PBD": "#008000",
"PTD": "#7fbf7f",
"DBD": "#800080",
"DTD": "#b266b2"
}

GENE_COLORS = ["#2d7e81", "#f6b25c", "#CFCAA5", "#AFC1C3"]
'''
TYPE_STRS = ["P_SE_BD", "P_SE_O", "P_BD_O", "P_O_O", "D_SE_BD", "D_SE_O", "D_BD_O", "D_O_O"]
TYPES = [128, 64, 32, 16, 8, 4, 2, 1]
INTRA_COLORS = {
"P_SE_BD": "#db6d00",
"P_SE_O": "#034f84",
"P_BD_O": "#008000",
"P_O_O": "#92a8d1",
"D_SE_BD": "#000000",
"D_SE_O": "#9D2932",
"D_BD_O": "#800080",
"D_O_O": "#b266b2"
}
TYPE_NAMES = TYPE_STRS
'''
