from __future__ import print_function
import os,sys
import numpy as np


def loadMapping(filename):

    mapping = {}
    with open(filename) as f:
        for r in f:
            r = r.strip()
            if r[0] == '#':
                continue
            tokens = r.split('\t')
            mapping[tokens[0]] = tokens[1]
    return mapping



def loadData( reader, bed_fields = 13 ):
    bed13 = []
    exprs = []
    for r in reader:
        if r[0] != '#':
            break
    header = r.strip().split('\t')
    print(header)
    for r in reader:
        tokens = r.strip().split('\t')
        bed13.append(tokens[:bed_fields])
        exprs.append(map(float, tokens[bed_fields:]))
    reader.close()
    return header,bed13,exprs


def get2DecimalStr( f ):
    return "%f" % f

def tukey_biweight(x, c=5, epsilon=0.0001):
    x = np.array(x)
    m = np.median(x)
    s = np.median(np.abs(x-m))
    u = (x-m) / (c*s + epsilon)
    w = np.zeros_like(x)
    i = np.abs(u) <= 1
    w[i] = ((1-u**2)**2)[i]
    bi = np.sum(w*x) / np.sum(w)
    return bi

def getEntropy( expr, use_tukey=True ):
    bins = np.zeros(19)
    expr_bins = []
    for e in expr:
        i = 0
        if e <= 0.000000001:
            i = 0
        else:
            v = np.log2(e)
            if v < -3:
                i = 0
            else:
                i = np.floor(v) + 4
        if i >= len(bins):
            i = len(bins) -1
        bins[i] += 1
        expr_bins.append(i)
    expr_prob = bins[expr_bins]

    etr = 0
    after_sum = 0
    if use_tukey:
        processed_expr = np.abs(np.array(expr) - tukey_biweight(expr))
    else:
        processed_expr = np.array(expr)
    total_expr = np.sum(processed_expr)
    for e in processed_expr:
        if e > 0.000000001:

            e = e / total_expr
            after_sum += e
            etr += -e * np.log2(e)
    return etr

def getSpeScore(x, use_tukey=True):
    spe_score = 1 - getEntropy(x, use_tukey)/np.log2(x.shape[0])
    return spe_score



def groupCols( header, mappings, exprs, features, dType, start_col=13, outNameAppendix = "", use_tukey=True):
    exprs = np.array( exprs )
    groups = {}
    new_header = []
    avg_exprs = []
    used = 0
    for i,h in enumerate(header[start_col:]):
        c_id = h.strip().split('.')[-1].split('_')[0]
        if i==0:
            print(c_id)
        if c_id in mappings:
            used += 1
            if mappings[ c_id ] not in groups:
                groups[ mappings[ c_id ] ] = []
            groups[ mappings[ c_id ] ].append(i)
    avg_exprs = np.zeros((exprs.shape[0], len(groups.keys())))
    i = 0
    for g in groups:
        new_header.append('exp.rle.' + g.replace(' ', '_'))
        avg_exprs[:,i] = np.average(exprs[ :, groups[ g ]], axis=1)
        i+=1

    if use_tukey:
        tukey_str = "_use_tukey"
    else:
        tukey_str = ""
    out = open("robust_facets{}{}{}.osc".format(dType, outNameAppendix, tukey_str),"w")
    out.write('\t'.join(header[:start_col]) + "\t")
    out.write('\t'.join(new_header) + '\n')
    for i in xrange(avg_exprs.shape[0]):
        if sum(avg_exprs[i]) <= 0:
            continue
        spe_score = getSpeScore(avg_exprs[i], use_tukey)
        features[i][4] = str(spe_score)
        out.write('\t'.join(features[i]) + "\t")
        out.write('\t'.join(map(get2DecimalStr, avg_exprs[i])) + '\n')
    out.close()
    print("Samples used:", used)


def getArgs():
    import argparse
    parser = argparse.ArgumentParser("Group the samples into facets and calculate a specificity score.")
    parser.add_argument("dtype", help="The type of samples. It can be _cells, _celllines, _tissues")
    parser.add_argument("bed_fields", type=int, help="The number of bed fields.")
    parser.add_argument("inFile", help="The input data file.")
    parser.add_argument("-a", "--appendix", default = "", help="The appendix to the output file.")
    parser.add_argument('--use_tukey', action="store_true", default=False, help="Whether to use tukey-biweight processed expression for calculation of specificity score. Defaults to False")
    args = parser.parse_args()

    return args

if __name__=='__main__':
    args = getArgs()
    dType = args.dtype
    bed_fields = args.bed_fields
    mappings = loadMapping("id_2_facet{}.txt".format(dType))
    print(dType)

    print("loading")
    header,features,exprs = loadData(open(args.inFile,'r'), bed_fields)
    print("Grouping")
    groupCols( header, mappings, exprs, features, dType, bed_fields, args.appendix, use_tukey=args.use_tukey )




