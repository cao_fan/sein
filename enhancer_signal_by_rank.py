
# coding: utf-8

import os,sys

import matplotlib
matplotlib.use("Agg")
from matplotlib import rc


rc('font', **{'family':'sans-serif','sans-serif':['Helvetica']})
#matplotlib.rcParams['ps.fonttype'] = 'none'
matplotlib.rcParams['svg.fonttype'] = 'none'
matplotlib.rcParams['savefig.jpeg_quality'] = 95
matplotlib.rcParams['axes.linewidth'] = 1
#matplotlib.rcParams['font.family'] = 'sans-serif'
#matplotlib.rcParams['font.sans-serif'] = ['Arial']
rc('text',usetex=True)
#rc('text.latex', preamble='\usepackage[dvipsnames]{xcolor}')
matplotlib.rcParams['text.latex.preamble'] = [
       r'\usepackage[dvipsnames]{xcolor}',
       r'\usepackage{siunitx}',   # i need upright \micro symbols, but you need...
       r'\sisetup{detect-all}',   # ...this to force siunitx to actually use your fonts
       r'\usepackage{helvet}',    # set the normal font here
       r'\usepackage{sansmath}',  # load up the sansmath so that math -> helvet
       r'\sansmath'               # <- tricky! -- gotta actually tell tex to use!
]
import matplotlib.pyplot as plt



import pylab as pl
import numpy as np
from decimal import *
import enhancer as ENHANCER, tss as TSS, interaction as INTERACTION, Overlap_super_enhancer_n_chiapet_lietal_SP as osp
import Sein
inch = 25.4
pageWidth = 8.27
pageHeight = 11.69
col1Width= 87/25.4
col2Width = 180/25.4
fontsize = 10
from collections import defaultdict


# In[ ]:

def plotSignal(y_data, numSuper, ax, broad=False):
    xlabel = "Enhancers ranked by H3K27ac signal" if not broad else "Domains ranked by size"
    ylabel = "H3k27ac signal (x1000)" if not broad else "Domain size (kb)"
    typ_str = "Typical"
    sup_str = "Super" if not broad else "Broad"
    ax.plot(y_data, 'r.')
    ax.plot(y_data, 'r-')
    ax.set_xticks(range(0,len(y_data), 5000))
    ax.set_xticklabels(range(0,len(y_data), 5000), fontsize=fontsize)
    ax.set_xlim([-1000, len(y_data) + 1000])
    ax.set_yticks(range(0,210001,20000))
    ax.set_ylim([-max(y_data)*0.1, int(float(max(y_data))*1.1)])
    ax.set_yticklabels(["%d" % (i/1000) for i in range(0,210001,20000)], fontsize=fontsize)
    ax.set_ylabel(ylabel,fontsize=14)
    ax.set_xlabel(xlabel,fontsize=14)
    ax.axhline(y=y_data[-numSuper], linestyle="--", color='grey')
    ax.text(len(y_data)*0.02,y_data[-numSuper]+len(y_data)*0.05, "Typical (%d)" % (len(y_data) - numSuper), ha="left", va="bottom", fontsize=fontsize)
    ax.text(len(y_data) - numSuper - 0.02*len(y_data), y_data[-1] * 1.07, "%s (%d)" % (sup_str, numSuper), va="top", ha="right", rotation=90, fontsize=fontsize)
    ax.axvline(x=len(y_data) - numSuper, linestyle='--', color='grey')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()


def collateSameGene(values, ignore_rnaseq=False):
    grouped_text = defaultdict(int)
    for x in values:
        grouped_text[x[0]] += x[1]

    grouped_text = list(grouped_text.items())
    grouped_text.sort(key=lambda k: k[1])
    if not ignore_rnaseq and len(grouped_text) > 0 and grouped_text[-1][1] == 0:
        grouped_text = []
    return grouped_text

def addAnnotation(ax, enhancers_flat, ogs, tsgs, ccgs, rnaseq_gene_expressions, signal_range,
                  numSuper, what="top", loop=True, broad=False, ignore_rnaseq=False, rnaseq_thresh=1):
    if ignore_rnaseq:
        rnaseq_thresh = -1
    top = True if what.lower() == "top" else False
    count = 0
    texts = []
    point_x = []
    point_y = []
    curr_tsg_count = 0
    partner_tsg_count = 0
    partner_add_count = 0
    add_count = 0
    N = 20
    tsg_space = signal_range / N
    temp = ax.text(len(enhancers_flat) - 3000,
                   (N + 4) * tsg_space + 100,
                   r"\textbf{By Proximity}",
                   ha="right", va="center", fontsize=fontsize)
    if loop:
        temp = ax.text(len(enhancers_flat) + 3000,
                       (N +4) * tsg_space + 100,
                       r"\textbf{By Looping}",
                       ha="left", va="center", fontsize=fontsize)

    def add_text(texts, is_left, curr_tsg_count):
        grouped_style_text = []
        for x, expr in texts:
            tempColor = "black"
            tempunderline = False
            if x in ogs and x in tsgs:
                tempColor = "Plum"
            elif x in ogs:
                tempColor = "Maroon"
            elif x in tsgs:
                tempColor = "NavyBlue"
            if x in ccgs:
                tempunderline = True
            if not ignore_rnaseq:
                curr_text = "%s (%s)" % (x, expr)
            else:
                curr_text = x
            if tempunderline:
                curr_text = r"\underline{%s}" % curr_text
            curr_text = r"\textcolor{%s}{%s}" % (tempColor, curr_text)

            grouped_style_text.append(curr_text)
        display_text = " ".join(grouped_style_text)
        if is_left:
            ax.text(len(enhancers_flat) - 3000,
                            (N - curr_tsg_count+2) * tsg_space + 100,
                            display_text,
                            ha="right", va="center", fontsize=fontsize)
            ax.arrow(len(enhancers_flat) - 2900, (N - curr_tsg_count+2) * tsg_space + 100,
                        2900 - count - 200, e_y - (N - curr_tsg_count+2) * tsg_space,
                        head_width=0.05, head_length=0.1, facecolor='grey', ec='grey')
        else:
            ax.text(len(enhancers_flat) + 3000,
                            (N - curr_tsg_count+2) * tsg_space + 100,
                            display_text,
                            ha="left", va="center", fontsize=fontsize)
            ax.arrow(len(enhancers_flat) + 2900, (N - curr_tsg_count+2) * tsg_space + 100,
                        -2900 - count + 200, e_y - (N - curr_tsg_count+2) * tsg_space,
                        head_width=0.05, head_length=0.1, facecolor='grey', ec='grey', clip_on=False)

    for e in reversed(enhancers_flat):
        if e.enhancer_type >= 4:
            e_y = e.treat_signal - e.ctrl_signal if not broad else e.end - e.start

            curr_og_values = []
            curr_tsg_values = []
            curr_ccg_values = []
            curr_all_values = []
            for t in e.getTsses():
                og_values, tsg_values, _, ccg_values, all_values = t.get_rnaseq_gene_expression_for_og_tsg(rnaseq_gene_expressions, ogs, tsgs, None, ccgs, rnaseq_thresh)
                curr_og_values += og_values
                curr_tsg_values += tsg_values
                curr_ccg_values += ccg_values
                curr_all_values += all_values
            curr_og_values = list(set(curr_og_values))
            curr_tsg_values = list(set(curr_tsg_values))
            curr_ccg_values = list(set(curr_ccg_values))
            curr_all_values = list(set(curr_all_values))

            partner_og_values = []
            partner_tsg_values = []
            partner_ccg_values = []
            partner_all_values = []
            for p in e.getPartners():
                temp_og_values = []
                temp_tsg_values = []
                temp_ccg_values = []
                temp_all_values = []
                for t in p.getTsses():
                    (og_values, tsg_values, _, ccg_values, all_values) = t.get_rnaseq_gene_expression_for_og_tsg(rnaseq_gene_expressions, ogs, tsgs, None, ccgs, rnaseq_thresh)

                    temp_og_values += collateSameGene(og_values, ignore_rnaseq)
                    temp_tsg_values += collateSameGene(tsg_values, ignore_rnaseq)
                    temp_ccg_values += collateSameGene(ccg_values, ignore_rnaseq)
                    temp_all_values += collateSameGene(all_values, ignore_rnaseq)

                partner_og_values += collateSameGene(temp_og_values, ignore_rnaseq)
                partner_tsg_values += collateSameGene(temp_tsg_values, ignore_rnaseq)
                partner_ccg_values += collateSameGene(temp_ccg_values, ignore_rnaseq)
                partner_all_values += collateSameGene(temp_all_values, ignore_rnaseq)
            partner_og_values = list(set(partner_og_values))
            partner_tsg_values = list(set(partner_tsg_values))
            partner_ccg_values = list(set(partner_ccg_values))
            partner_all_values = list(set(partner_all_values))
            if top :
                if len(curr_all_values) > 0:
                    grouped_text = defaultdict(int)
                    for x in curr_all_values:
                        grouped_text[x[0]] += x[1]
                    grouped_text = list(grouped_text.items())
                    grouped_text.sort(key=lambda k: k[1])
                    add_text(grouped_text[-2:], True, curr_tsg_count)

                if loop and len(partner_all_values) > 0:
                    grouped_text = defaultdict(int)
                    for x in partner_all_values:
                        grouped_text[x[0]] += x[1]
                    grouped_text = list(grouped_text.items())
                    grouped_text.sort(key=lambda k: k[1])
                    add_text(grouped_text[-2:], False, curr_tsg_count)

                add_count += 1
                curr_tsg_count += 1
            else:
                if len(curr_og_values) + len(curr_tsg_values) + len(curr_ccg_values) > 0:
                    grouped_text = defaultdict(int)
                    for x in set(curr_og_values + curr_tsg_values + curr_ccg_values):
                        grouped_text[x[0]] += x[1]
                        
                    #print grouped_text
                    grouped_text = list(grouped_text.items())
                    add_text(grouped_text, True, curr_tsg_count)
                    curr_tsg_count += 1
                    add_count += 1

                if loop and len(partner_og_values) + len(partner_tsg_values) + len(partner_ccg_values) > 0:
                    grouped_text = defaultdict(int)
                    for x in set(partner_og_values + partner_tsg_values + partner_ccg_values):
                        grouped_text[x[0]] += x[1]
                        
                    #print grouped_text
                    grouped_text = list(grouped_text.items())
                    add_text(grouped_text, False, partner_tsg_count)
                    partner_tsg_count += 1
                    partner_add_count += 1

        count += 1
        if add_count >= 20:
            break



# In[ ]:

if __name__=='__main__':
    fd = os.path.dirname(os.path.realpath(__file__)) + "/data/"

    for cell in ['mcf7','k562']:
        for broad in [False, True]:
            outName = "revision_%s_h3k27ac" % cell if not broad else "revision_%s_h3k4me3" % cell

            #classified_root = "/home/caofan/projects/super_enhancer/k562/test/refseq/gencode_final_analysis/revision_tsl3/"


            interFiles = [fd + "%s_rep0.merged.intra.bedpe" % cell]
            isBedpes = [True,]
            interactions = []
            for interFile, bedpe in zip(interFiles, isBedpes):
                interactions += INTERACTION.loadInteraction(interFile, bedpe=bedpe)
            tssFile = fd + "gencode.v19.annotation_capped_sites_nr_with_confidence.bed"
            geneNameMapFile = fd + "gencode.v19.transcript_genename_hgnc_map.txt"
            tsses = TSS.loadTssFromBed(tssFile, TSS.makeNameMapDict(geneNameMapFile))
            enhancers = ENHANCER.loadEnhancersFromROSETable(fd+"ses/revision_%s_h3k27ac_AllEnhancers.table.txt" % cell,
                                            by_chrom=True, tsses = tsses, tss_dist = 4000) if not broad \
                        else ENHANCER.loadGappedPeaks(fd+'ses/%s_h3k4me3_optimal_broad_peaks.gappedPeak' % cell,
                                                      by_chrom=True, tsses = tsses, tss_dist = 4000)
            osp.make_counts(enhancers, interactions)
            ogs = Sein.loadGenes(fd + "og.txt")
            tsgs = Sein.loadGenes(fd + "tsg.txt")
            ccgs = Sein.loadGenes(fd + "ccg_leukemia.txt" if cell == 'k562' else fd+'ccg_breast.txt')

            rnaseq_gene_expression_files = [fd + "rnaseq/ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep1_Techrep1_ENCFF293IVM.tsv",
                      fd + "rnaseq/ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep1_Techrep1_ENCFF363JRF.tsv",
                      fd + "rnaseq/ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep2_Techrep1_ENCFF187BSB.tsv",
                      fd + "rnaseq/ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep2_Techrep1_ENCFF976JSC.tsv"] \
                      if cell == "k562" else [
                        fd + 'rnaseq/ENCFF615MUV.tsv',
                        fd + 'rnaseq/ENCFF703QWZ.tsv'
                    ]

            rnaseq_gene_expressions = Sein.load_rnaseq_levels(rnaseq_gene_expression_files, 0, 6)
            #rnaseq_gene_expressions = None
            ignore_rnaseq = False

            spans = [(i.start2+i.end2)/2 - (i.start1+i.end1)/2 for i in interactions if i.chrom1 == i.chrom2]

            for what in ['top', 'cancer']:
                for loop in [False, True]:
                    enhancers_flat = []
                    for k in enhancers:
                        enhancers_flat += enhancers[k]
                    if not broad:
                        enhancers_flat.sort(key=lambda k: k.treat_signal - k.ctrl_signal)
                    else:
                        enhancers_flat.sort(key=lambda k: k.end - k.start)
                    y_data = [e.treat_signal - e.ctrl_signal for e in enhancers_flat] if not broad else [e.end - e.start for e in enhancers_flat]
                    numSuper = len([e for e in enhancers_flat if e.enhancer_type >= 4])
                    fig = pl.figure(figsize=(col1Width*2, col1Width*2))

                    ax = fig.add_subplot(1,1,1)
                    plotSignal(y_data, numSuper, ax, broad=broad)
                    signal_range = y_data[-1] - y_data[-numSuper]
                    addAnnotation(ax, enhancers_flat, ogs, tsgs, ccgs, rnaseq_gene_expressions, signal_range, numSuper, 
                                    what=what, loop=loop, broad=broad, ignore_rnaseq=ignore_rnaseq)

                    loop_str = "loop" if loop else "noloop"
                    fig.savefig("%s_%s_%s.eps" % (outName,what,loop_str), bbox_inches="tight")
                    #fig.savefig("%s_%s_%s.png" % (outName,what,loop_str), bbox_inches="tight", dpi=400)
                    #fig.savefig("%s_%s_%s.pdf" % (outName,what,loop_str), bbox_inches="tight")
            plt.cla()



