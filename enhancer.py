from common import *
import tss as TSS

class Enhancer:
    def __init__(self, name, chrom, start, end, gene, rank, enhancer_type,
    treat_signal = 1, ctrl_signal = 0, num_loci=1, cons_size=1, cons_signal=1, cons_ctrl_signal=0, is_super=0,  extension=0):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.name = name
        self.rank = rank
        self.enhancer_type = enhancer_type  #3 bits: 1 for TP, 2 for TE, 4 for SP, 8 for SE
        self.treat_signal = treat_signal
        self.ctrl_signal = ctrl_signal
        self.num_loci = num_loci
        self.cons_signal = cons_signal
        self.cons_ctrl_signal = cons_ctrl_signal
        self.cons_size = cons_size
        self.is_super = is_super
        self.extension = extension #to store the extension

        self.interactions = []
        self.inter_sides = [] #bool values, left is True, right is False

        self.clusters = []
        self.tsses = []
        '''
        I: intra enhancer interaction counts, exclusive to others.
                #P: mate is promoter
                #T: mate is typical_enhancer
                #PT: mate has both promoter and typical_enhancer
                #S: mate is super-enhancer
                #PS: mate has both promoter and super-enhancer
                #TS: mate has both typical- and super-enhancers
                #A: mate has promoter, typical-enhancer, and super-enhancer
        INTER: inter-chromosomal interactions, not exclusive.
        '''
        self.scores = {"I": 0, "INTER": 0}

        #For complexes
        self.in_complex = False

    def getLength(self):
        return self.end - self.start

    def setUsed(self):
        self.in_complex = True

    def isUsed(self):
        return self.in_complex

    def extend(self, ext_size):
        self.start -= int(ext_size/2)
        self.end += int(ext_size/2)

    def increment(self, key ):
        if key not in self.scores:
            self.scores[ key ] = 0
        self.scores[ key ] += 1

    def getType( self ):
        return self.enhancer_type

    def getTypeStr( self ):
        if self.enhancer_type == TYPE_SE:
            return TYPE_NAMES[TYPES.index(TYPE_SE)]
        elif self.enhancer_type == TYPE_SP:
            return TYPE_NAMES[TYPES.index(TYPE_SP)]
        elif self.enhancer_type == TYPE_TE:
            return TYPE_NAMES[TYPES.index(TYPE_TE)]
        elif self.enhancer_type == TYPE_TP:
            return TYPE_NAMES[TYPES.index(TYPE_TP)]
        else:
            return None

    def getName( self ):
        return self.name

    def addInteraction( self, inter, side ):
        self.interactions.append( inter )
        self.inter_sides.append( side )

    def getInteractions( self ):
        return self.interactions, self.inter_sides

    def getPartners( self, e_type = 0, is_complex = False ):
        partners = set()
        for inter,side in zip (self.interactions, self.inter_sides):
            if is_complex and inter.isUsed():
                continue
            if side:
                others = inter.getRightEnhancers()
            else:
                others = inter.getLeftEnhancers()
            for o in others:
                if is_complex and o.isUsed():
                    continue
                if e_type <= 0 or o.getType() == e_type:
                    partners.add(o)
        return list(partners)

    def getAllAnchorElements( self, e_type = 0, is_complex = False ):
        '''
        Find all elements that share at least 1 anchor with the current enhancer.
        Return a list of elements including the enhancer itself.
        '''
        eles = set()
        for inter,side in zip(self.interactions, self.inter_sides):
            if is_complex and inter.isUsed():
                continue
            if side:
                neighs = inter.getLeftEnhancers()
            else:
                neighs = inter.getRightEnhancers()
            for o in neighs:
                if e_type <= 0 or o.getType() == e_type:
                    eles.add(o)
        return list(eles)

    def numIntra( self ):
        return self.scores['I']

    def numInterChrom( self ):
        return self.scores["INTER"]

    def getNumInterByType( self, mate_type ):
        count = 0
        for e_type in self.scores:
            if mate_type in e_type:
                count += self.scores[ e_type ]
        return count

    def numPartner_SE( self ):
        #return len( self.getPartners( e_type = TYPE_SE ))
        #return self.scores['S'] + self.scores['PS'] + self.scores['TS'] + self.scores['A']
        return self.getNumInterByType( TYPE_STRS[TYPES.index(TYPE_SE)] )


    def numPartner_SP( self ):
        #return len( self.getPartners( e_type = TYPE_SP ))
        return self.getNumInterByType( TYPE_STRS[TYPES.index(TYPE_SP)] )

    def numPartner_TE( self ):
        #return len( self.getPartners( e_type = TYPE_TE ))
        #return self.scores['T'] + self.scores['PT'] #+ self.scores['TS'] + self.scores['A']
        return self.getNumInterByType( TYPE_STRS[TYPES.index(TYPE_TE)] )

    def numPartner_TP( self ):
        #return len( self.getPartners( e_type = TYPE_TP ))
        #return self.scores['P'] #+ self.scores['PS'] + self.scores['PT'] + self.scores['A']
        return self.getNumInterByType( TYPE_STRS[TYPES.index(TYPE_TP)] )

    def getClusters( self ):
        return self.clusters

    def addCluster( self, c ):
        self.clusters.append(c)

    def addTss(self, t):
        self.tsses.append(t)

    def addTsses(self, ts):
        self.tsses += ts

    def getTsses(self):
        return self.tsses

    def getTssClusters(self):
        tempClusters = []
        tempNames = set()
        for t in self.tsses:
            for cluster in t.getClusters():
                if cluster.name not in tempNames:
                    tempClusters.append(cluster)
                    tempNames.add(cluster.name)
        return tempClusters

    def getEnhancerClusters(self):
        tempClusters = []
        for cagec in self.clusters:
            if cagec.isEnhancer():
                tempClusters.append(cagec)
        return tempClusters

    def asBedStr( self ):
        return "\t".join([self.chrom, str(self.start + self.extension), str(self.end - self.extension), self.name, str( self.treat_signal - self.ctrl_signal), "."])


def loadEnhancersFromROSETable( filename, classified=[], classified_types=[], tsses = None, by_chrom=True, tss_dist = 0 ):
    assert len(classified) == len(classified_types)

    def getNameTypeMapping( classified, classified_types ):
        mapping = {}
        for fn,curr_type in zip(classified, classified_types):
            f = open(fn)
            for r in f:
                if r != '' and r[0] != '#':
                    mapping[r.strip().split('\t')[3]] = curr_type
            f.close()
        return mapping

    mapping = getNameTypeMapping(classified, classified_types)

    f = open(filename)
    enhancers = {}
    if not by_chrom:
        enhancers = []

    for r in f:
        if r[0] == '#' or r.startswith("REGION"):
            continue
        tokens = r.strip().split('\t')
        en = Enhancer(tokens[0], tokens[1], int(tokens[2]), int(tokens[3]), '.', int(tokens[10]), 0, treat_signal=float(tokens[6]), ctrl_signal=float(tokens[7]),num_loci=int(tokens[4]),cons_size=int(tokens[5]),cons_signal=float(tokens[8]),cons_ctrl_signal=float(tokens[9]), is_super=int(tokens[11]))
        currTsses = TSS.findNearestTss(en.chrom, en.start, en.end, tsses, overlap=True, center_dist=tss_dist)
        en.addTsses(currTsses)
        if len(currTsses) > 0:
            en.enhancer_type = TYPE_TP if en.is_super == 0 else TYPE_SP
        else:
            en.enhancer_type = TYPE_TE if en.is_super == 0 else TYPE_SE
        if by_chrom:
            if tokens[1] not in enhancers:
                enhancers[tokens[1]] = []
            enhancers[tokens[1]].append(en)
        else:
            enhancers.append(en)
    f.close()
    if by_chrom:
        for key in enhancers:
            enhancers[key].sort(key=lambda k:(k.start, k.end))
    else:
        enhancers.sort(key=lambda k:(k.chrom, k.start, k.end))
    return enhancers

def loadGappedPeaks(filename, tsses=None, by_chrom=True, tss_dist=0):
    f = open(filename)
    enhancers = {}
    for r in f:
        if r[0] == '#' or r.startswith('REGION'):
            continue
        tokens = r.strip().split('\t')
        '''
    def __init__(self, name, chrom, start, end, gene, rank, enhancer_type,
    treat_signal = 1, ctrl_signal = 0, num_loci=1, cons_size=1, cons_signal=1, cons_ctrl_signal=0, is_super=0,  extension=0):
        '''
        en = Enhancer(tokens[3], tokens[0], int(tokens[1]),int(tokens[2]),'.',0,0,
                treat_signal=float(tokens[12]), num_loci=int(tokens[9]))
        if tsses is not None:
            currTsses = TSS.findNearestTss(en.chrom, en.start, en.end, tsses, overlap=True, center_dist=tss_dist)
            en.addTsses(currTsses)

            if len(currTsses) > 0:
                en.enhancer_type = TYPE_TP
            else:
                en.enhancer_type = TYPE_TE
        if by_chrom:
            if tokens[0] not in enhancers:
                enhancers[tokens[0]] = []
            enhancers[tokens[0]].append(en)
        else:
            enhancers.append(en)
    f.close()
    enhancers_flat = []
    if by_chrom:
        for key in enhancers:
            enhancers_flat += enhancers[key]
    else:
        enhancers_flat = enhancers

    enhancers_flat.sort(key=lambda k: k.start - k.end)
    numSuper = int(len(enhancers_flat) * 0.05)
    for i in range(numSuper):
        enhancers_flat[i].enhancer_type = TYPE_SP if enhancers_flat[i].enhancer_type == TYPE_TP else TYPE_SE
    if by_chrom:
        for key in enhancers:
            enhancers[key].sort(key=lambda k:(k.start, k.end))
    else:
        enhancers.sort(key=lambda k:(k.chrom, k.start, k.end))
    return enhancers

def __parseBed( reader, enhancer_type, extension, container, count, names ):
    '''
    This is to load the bed files.
    '''
    for r in reader:
        r = r.strip()
        if r != '' and r[0] != '#':
            tokens = r.split('\t')
            en = Enhancer(tokens[3], tokens[0], int(tokens[1]) - extension, int(tokens[2]) + extension, ".",
                  count, enhancer_type, treat_signal=1, ctrl_signal=0, extension = extension)
            if tokens[0] not in container:
                container[tokens[0]] = []
            if tokens[3] not in names:
                container[tokens[0]].append(en)
                names.add( tokens[3] )
            count += 1
    return count



def loadEnhancersFromBedList( filenames, e_types, use_extension=False ):
    '''
    Load a enhancers from a list of filenames and corresponding types.
    '''
    if len(filenames) != len(e_types):
        print("Filenames and types are not equal")
        return

    enhancers = {}
    count = 1
    names = set()
    sizes = {}
    for t in TYPES:
        sizes[t] = []
    for filename, e_type in zip(filenames,e_types):
        f = open( filename )
        count = __parseBed( f, e_type, 0, enhancers, count, names )
    for key in enhancers:
        enhancers[key].sort(key=lambda k:(k.start,k.end))
        for e in enhancers[key]:
            sizes[ e.enhancer_type ].append( e.end - e.start )
    max_size = 0
    type_avg_sizes = {}
    for t in sizes:
        temp_size = sum(sizes[t])*1.0 / len(sizes[t])
        if temp_size > max_size:
            max_size = temp_size
        type_avg_sizes[t] = temp_size
    if use_extension:
        for key in enhancers:
            for e in enhancers[key]:
                e.extend( max_size - type_avg_sizes[e.enhancer_type] )

    f.close()
    return enhancers

def mergeEnhancerLists( enhancers1, enhancers2 ):
    chroms = set( enhancers1.keys() + enhancers2.keys() )
    overlappingChroms = set( enhancers1.keys() ).intersection( set( enhancers2.keys() ))
    merged = {}
    for c in overlappingChroms:
        merged[c] = []
        i = 0
        j = 0
        while i < len(enhancers1[c]) and j < len(enhancers2[c]):
            e1 = enhancers1[c][i]
            e2 = enhancers2[c][j]
            if e1.start < e2.start or (e1.start == e2.start and e1.end < e2.end ):
                merged[c].append( e1 )
                i += 1
            else:
                merged[c].append( e2 )
                j += 1
        while i < len(enhancers1[c]):
            merged[c].append( enhancers1[c][i] )
            i+=1
        while j < len(enhancers2[c]):
            merged[c].append( enhancers2[c][j] )
            j+=1
    for c in chroms:
        if c not in overlappingChroms:
            if c in enhancers1:
                merged[c] = enhancers1[c]
            else:
                merged[c] = enhancers2[c]
    return merged


def writeEnhancersBasedOnPartner( enhancers, filenamePre, precedence="SP-TP-SE-TE" ):

    tokens = precedence.upper().split('-')
    print(tokens)
    type_to_token = {TYPE_SP: tokens.index('SP'), TYPE_TP: tokens.index('TP'), TYPE_SE: tokens.index('SE'), TYPE_TE: tokens.index('TE')}
    token_to_type = {'SP': TYPE_SP, 'TP': TYPE_TP, 'SE': TYPE_SE, 'TE': TYPE_TE}
    p_types = [token_to_type[i] for i in tokens]
    outs = []
    for t in tokens:
        for h in tokens:
            outs.append(open("%s_%s-%s_%s" % (filenamePre, t, h, precedence), 'w'))
        outs.append(open("%s_%s_NonInter_%s" % (filenamePre, t, precedence), 'w'))
    for k in enhancers:
        for e in enhancers[k]:


            token_idx = type_to_token[ e.getType() ]
            token1 = tokens[ token_idx ]
            token2 = None
            bedScore = 0
            partner_names = []

            all_scores = [e.getNumInterByType(t) for t in tokens]
            all_scores.append(e.numIntra())

            for i,x in enumerate(all_scores[:4]):
                if x > 0:
                    token2 = tokens[i]
                    bedScore = x
                    for p in e.getPartners( e_type = p_types[i] ):
                        if p.name != e.name:
                            partner_names.append( p.name )
                    break

            if token2 != None:
                idx = tokens.index( token2 )
                all_scores[ idx ] = "%d:%s" % (all_scores[ idx ], ",".join( partner_names ))
                all_scores_str = [all_scores[tokens.index('SE')], all_scores[tokens.index('SP')],
                        all_scores[tokens.index('TE')], all_scores[tokens.index('TP')], all_scores[-1]]

                outs[ token_idx * (len(tokens) + 1) + tokens.index( token2 ) ].write( "%s\t%s\n" % \
                                                                                       (e.asBedStr(), "\t".join(map(str, all_scores_str))) )
            else:
                all_scores_str = [all_scores[tokens.index('SE')], all_scores[tokens.index('SP')],
                        all_scores[tokens.index('TE')], all_scores[tokens.index('TP')], all_scores[-1]]
                outs[ token_idx * (len(tokens) + 1)  + len(tokens) ].write( "%s\t%s\n" % \
                                                                            (e.asBedStr(), "\t".join(map(str, all_scores_str))) )
    for o in outs:
        o.close()

