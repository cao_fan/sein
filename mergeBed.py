import argparse


def parse_args():
    parser = argparse.ArgumentParser("Merge sorted bed files based on their overlapping properties.")

    parser.add_argument("input", help="The bed file sorted by coordinates.")
    parser.add_argument("output", help="The output files.")
    parser.add_argument("-s", "--strand", action="store_true", default=False,
                        help="Force strandness. Default: False")
    parser.add_argument("-f", "--fraction", type=float, default=0,
                        help="The size of overlapping required as a fraction of the smaller feature. Default: 0")
    parser.add_argument("-d", "--distance", type=int, default=0,
                        help="The distance allowed between two records to merged. Default: 0.")

    return parser.parse_args()


def overlap(x, y, require_strand, fraction_allowed, distance_allowed):
    if x[0] != y[0] or (require_strand and x[5] != y[5]):
        return False
    dist_x_y = max(x[1], y[1]) - min(x[2], y[2])
    if dist_x_y < distance_allowed and abs(dist_x_y) >= fraction_allowed * min(x[2]-x[1], y[2]-y[1]):
        return True


def do_work():
    args = parse_args()

    f = open(args.input)
    out = open(args.output, 'w')

    last_record = None
    last_names = None
    last_strands = None
    last_scores = None
    for r in f:
        tokens = r.strip().split()
        tokens[1] = int(tokens[1])
        tokens[2] = int(tokens[2])
        tokens[4] = int(tokens[4])
        if last_record is None:
            last_record = tokens
            last_names = [tokens[3],]
            last_strands = [tokens[5],]
            last_scores = [tokens[4],]
        else:
            if overlap(last_record, tokens, args.strand, args.fraction, args.distance):
                last_record[1] = min(tokens[1], last_record[1])
                last_record[2] = max(tokens[2], last_record[2])
                last_names.append(tokens[3])
                last_strands.append(tokens[5])
                last_scores.append(tokens[4])
            else:
                if len(set(last_strands)) > 1:
                    last_record[5] = "."
                last_record[3] = ";".join(last_names)
                last_record[4] = sum(last_scores) / len(last_scores)
                out.write('\t'.join(map(str, last_record)) + "\n")

                last_record = tokens
                last_names = [tokens[3], ]
                last_strands = [tokens[5], ]
                last_scores = [tokens[4], ]
    if len(set(last_strands)) > 1:
        last_record[5] = "."
    last_record[3] = ";".join(last_names)
    last_record[4] = sum(last_scores) / len(last_scores)
    out.write('\t'.join(map(str, last_record)) + "\n")

    f.close()
    out.close()


if __name__=="__main__":
    do_work()
