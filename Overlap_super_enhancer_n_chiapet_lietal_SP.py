# -*- coding: utf-8 -*-

from __future__ import print_function
import os,sys

import bisect
import re
import pylab
import numpy as np
import prettyplotlib as ppl
import itertools
from common import *
import enhancer
import interaction
import cluster


def hist_wrapper( ax, data, bins, normed = False, color='b', rwidth = 0.6, fontsize=8 ):
    counts, bins = np.histogram( np.array(data), bins=bins )
    binCenters = 0.5 * (bins[1:] + bins[:-1])
    rects = ppl.bar(ax, binCenters - rwidth/2, counts, color=color, grid='y', width=rwidth)
    autolabel(rects, ax, fontsize=fontsize)

def autolabel(rects, ax, fontsize=8):
    # attach some text labels
    print(len(rects))
    for rect in rects:
        height = rect.get_height()
        cstr = ""
        offset = ax.get_ylim()[1] * 0.001
        if int(height) == float(height):
            cstr = "%d"%int(height)
        else:
            cstr = "%.2f"%float(height)
        ax.text(rect.get_x()+rect.get_width()/2., height + offset, cstr,
                ha='center', va='bottom', fontsize=fontsize)

def make_counts(enhancers, interactions):
    import bisect
    count = [] #The number of enhancers covered by each left anchor
    count_2 = [] #The number of enhancers covered by each right anchor
    type_count = {}
    for i in range(len(interactions)):
        inter = interactions[i]
        left_starts = [k.start for k in enhancers[inter.chrom1]]
        left_ends = [k.end for k in enhancers[inter.chrom1]]
        right_starts = [k.start for k in enhancers[inter.chrom2]]
        right_ends = [k.end for k in enhancers[inter.chrom2]]
        left_is = bisect.bisect_left(left_ends, inter.start1)
        left_ie = bisect.bisect_right( left_starts, inter.end1)
        #NOTE: This only holds when no region is contained within another region.
        #Otherwise, it might not behave correctly.
        count.append( left_ie - left_is )
        right_is = bisect.bisect_left(right_ends, inter.start2)
        right_ie = bisect.bisect_right( right_starts, inter.end2)
        count_2.append( right_ie - right_is)
        if count[-1] > 0 and count_2[-1] > 0:
            is_intra = False
            left_enhancers = enhancers[ inter.chrom1 ][ left_is: left_ie ]
            right_enhancers = enhancers[ inter.chrom2 ][ right_is: right_ie ]

            ## Check whether the interaction is intra
            if not inter.isInterChrom():
                if min( left_ie, right_ie ) > max( left_is, right_is ):
                    is_intra = True
                    tempId = max( left_is, right_is ) #only one enhancer can have the intra
                    tempEnhancer = left_enhancers[ tempId - left_is ]
                    tempEnhancer.increment( 'I' )
                    inter.setLeftEnhancers( [tempEnhancer,])
                    inter.setRightEnhancers( [tempEnhancer,])

                    inter.setIntra( tempEnhancer.getType() )
                    tempEnhancer.addInteraction( inter, True )
                    continue

            for left_e in left_enhancers:
                left_e.addInteraction( inter, True )

            for right_e in right_enhancers:
                right_e.addInteraction( inter, False )

            for left_e in left_enhancers:
                inter.addLeftScore( left_e.getType() )

            for right_e in right_enhancers:
                inter.addRightScore( right_e.getType() )

            for left_e in left_enhancers:
                left_e.increment( inter.getRightScoreStr() )

            for right_e in right_enhancers:
                right_e.increment( inter.getLeftScoreStr() )

            if inter.isInterChrom():
                for left_e in left_enhancers:
                    left_e.increment( "INTER" )
                for right_e in right_enhancers:
                    right_e.increment( "INTER" )

            inter.setLeftEnhancers( left_enhancers )
            inter.setRightEnhancers (right_enhancers )

    return count, count_2, type_count


def writeDot( enhancers, figNamePre, write_all = False ):
    import graphviz
    dot = graphviz.Graph(comment="Enhancers", format="svg", engine="circo")
    colors = {TYPE_TE: "cyan", TYPE_SE: "red", TYPE_TP: "green"}
    for key in enhancers:
        for e in enhancers[key]:
            if e.getType() == TYPE_SE or write_all:
                curr_interactions, curr_sides = e.getInteractions()
                if len(curr_interactions) <= 0:
                    continue
                dot.node(e.getName(), tooltip="%s:%d-%d" % (e.chrom, e.start, e.end), color = colors[e.getType()])

                for inter, side in zip(curr_interactions, curr_sides):
                    #Enhancers are sorted by position, thus only use the left as the reference.
                    if side: #if left
                        for oe in inter.getRightEnhancers():
                            #print("add edge")
                            dot.node(oe.getName(), tooltip = "%s:%d-%d"%(oe.chrom, oe.start, oe.end), \
                                     color=colors[oe.getType()])
                            dot.edge(e.getName(), oe.getName(), tooltip = inter.asOrigStr())
    extraStr = ''
    if write_all:
        extraStr = '_all'
    dot.render("%s%s.gv"% (figNamePre, extraStr), view=True)


def writeInteractionByType( interactions, filenamePre ):
    type_to_idx = {}
    int_type_counts = {}
    idx_tracker = 0
    int_filenames = []
    for e in itertools.product(range(len(TYPE_STRS)), range(len(TYPE_STRS))):
        if e[0] <= e[1]:
            tempKey = "%s-%s" % ( TYPE_STRS[e[0]], TYPE_STRS[e[1]] )
            type_to_idx[ tempKey ] = idx_tracker
            int_type_counts[ tempKey ] = 0
            idx_tracker += 1
            int_filenames.append("%s_%s_interactions_byprogram" % (filenamePre, tempKey))
    outs = [open(f,'w') for f in int_filenames]

    for inter in interactions:
        currTypeStr = inter.getType(True)
        if currTypeStr in type_to_idx:
            outs[type_to_idx[currTypeStr]].write(inter.asBEDPEStr() + "\n")
            int_type_counts[currTypeStr] += 1


    for o in outs:
        o.close()

def getComplexes( enhancers ):
    complexes = []
    for k in enhancers:
        for e in enhancers[k]:
            if not e.isUsed():
                temp_complex = []
                curr_complex = []
                temp_complex += e.getAllAnchorElements( is_complex = True )
                temp_complex += e.getPartners( is_complex = True)
                temp_interactions = e.getInteractions()[0]
                for inter in temp_interactions:
                    if not inter.isUsed():
                        inter.setUsed()
                while len(temp_complex) > 0:
                    pop_e = temp_complex.pop()
                    if pop_e.isUsed():
                        continue
                    temp_complex += pop_e.getAllAnchorElements( is_complex = True )
                    temp_complex += pop_e.getPartners( is_complex = True )
                    temp_interactions = pop_e.getInteractions()[0]
                    for inter in temp_interactions:
                        if not inter.isUsed():
                            inter.setUsed()

                    pop_e.setUsed()
                    curr_complex.append(pop_e)
                if len(curr_complex) > 0:
                    complexes.append(curr_complex)
    return complexes
