from common import *
class Interaction:
    def __init__(self, chrom1, start1, end1, chrom2, start2, end2, support):
        self.chrom1 = chrom1
        self.start1 = start1
        self.end1 = end1
        self.chrom2 = chrom2
        self.start2 = start2
        self.end2 = end2
        self.support = support
        #self.scoreToType = [None, "P", "T", "PT", "S", "PS", "TS", "A"]

        ###Scores
        self.intra = 0 #3 bits, 0: no intra, 1: intra promoter, 2: intra typical, 4: intra super promoter, 8: intro super enhancer
        self.left_score = 0 #3 bits, 0: no enhancer, 1: has promoter, 2: has typical, 4: has super promoter, 8: has super_enhancer
        self.right_score = 0
        self.left_enhancers = []
        self.right_enhancers = []

        #For use in making complexes
        self.used_in_complex = False


    def setUsed( self ):
        self.used_in_complex = True

    def isUsed( self ):
        return self.used_in_complex

    def __scoreToStr( self, score, largestOnly = False ):
        scoreStr = []
        found = False
        if score & TYPE_SE == TYPE_SE:
            scoreStr.append("DSE")
            found = True
        if score & TYPE_SP == TYPE_SP and not ( found and largestOnly ):
            scoreStr.append("PSE")
            found = True
        if score & TYPE_TE == TYPE_TE and not ( found and largestOnly ):
            scoreStr.append("DTE")
            found = True
        if score & TYPE_TP == TYPE_TP and not ( found and largestOnly ):
            scoreStr.append("PTE")
            found = True
        if len( scoreStr ) <= 0:
            return "None"
        return "_".join( scoreStr )


    def getLeftScoreStr( self ):
        return self.__scoreToStr( self.getLeftScore() )

    def getRightScoreStr( self ):
        return self.__scoreToStr( self.getRightScore() )

    def getLeftScore( self ):
        return self.left_score

    def getRightScore( self ):
        return self.right_score

    def __add_score(self, score, value ):
        return score | value

    def addLeftScore(self, value ):
        self.left_score = self.__add_score(self.left_score, value)

    def addRightScore(self, value ):
        self.right_score = self.__add_score( self.right_score, value)


    def getType( self, largestOnly = False ):

        firstScore = self.left_score
        secondScore = self.right_score
        if firstScore < secondScore:
            firstScore = self.right_score
            secondScore = self.left_score
        return self.__scoreToStr( firstScore, largestOnly ) + "-" + self.__scoreToStr( secondScore, largestOnly )


    def isInterChrom( self ):
        return self.chrom1 != self.chrom2

    def setIntra( self, value ):
        self.intra = value

    def setLeftEnhancers(self, enhancers):
        self.left_enhancers = enhancers

    def setRightEnhancers(self, enhancers):
        self.right_enhancers = enhancers

    def getLeftEnhancers( self ):
        return self.left_enhancers

    def getRightEnhancers( self ):
        return self.right_enhancers

    def getDistance( self ):
        if self.isInterChrom():
            return -1
        else:
            return self.start2 - self.end1

    def asOrigStr( self ):
        return "%s:%d..%d-%s:%d..%d,%d" % (self.chrom1, self.start1, self.end1, self.chrom2, self.start2, self.end2, self.support)

    def asBEDPEStr( self ):
        return "%s\t%d\t%d\t%s\t%d\t%d\t%s" % (self.chrom1, self.start1, self.end1, self.chrom2, self.start2, self.end2, self.asOrigStr() )


def loadInteraction( filename, support = 0, bedpe=False ):
    f = open( filename )
    reading_sec = False
    interactions = set()
    results = []
    if bedpe:
        for r in f:
            r = r.strip()
            if r[0] == '#' or r.startswith("track"):
                continue
            else:
                tokens = r.split('\t')
                intCols = [1,2,4,5,7]
                for i in intCols:
                    tokens[i] = int(tokens[i])
                outCols = [0,1,2,3,4,5,7]
                results.append(Interaction(*[tokens[i] for i in outCols]))

    else:
        nameCol = 3
        for r in f:
            r = r.strip()
            if r[0] == '#' or r.startswith("track"):
                continue
            else:
                tokens = r.split('\t')
                interactions.add(tokens[nameCol])

        f.close()
        import re
        pat = '(?P<chr_1>\S+):(?P<start_1>\d+)\.\.(?P<end_1>\d+)-(?P<chr_2>\S+):(?P<start_2>\d+)\.\.(?P<end_2>\d+),(?P<count>\d+).*'
        for inter in interactions:
            temp = list(re.match(pat, inter).groups())

            intCols = [1,2,4,5,6]
            for i in intCols:
                temp[i] = int(temp[i])
            if temp[6] >= support:
                results.append( Interaction(*temp) )
    results.sort(key=lambda k:(k.chrom1,k.start1,k.start2))
    return results
