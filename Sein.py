# -*- coding: utf-8 -*-


from __future__ import print_function
import sys, os
import matplotlib

matplotlib.use('Agg')
matplotlib.rcParams['svg.fonttype'] = 'none'
matplotlib.rcParams['savefig.jpeg_quality'] = 95
matplotlib.rcParams['axes.linewidth'] = 1
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = ['Arial']



import cluster as CLUSTER, enhancer as ENHANCER, interaction as INTERACTION
import tss as TSS, Overlap_super_enhancer_n_chiapet_lietal_SP as osp
import numpy as np

from common import *
import pylab as pl
from scipy.stats import kruskal, f_oneway
import dunn
from itertools import combinations, product

inch = 25.4
col1Width = 87 / inch
col2Width = 180 / inch



tick_fontsize = 12
axislabel_fontsize = 14
title_fontsize = 16


def set_tickFontSize(axis, fs):
    for tick in axis.get_major_ticks():
        tick.label.set_fontsize(fs)


def writeSepEnhancerBed(enhancers, prefix, appendix):
    outs = {}
    for t,s in zip([TYPE_SE, TYPE_SP, TYPE_TE, TYPE_TP],["SE", "SP", "TE", "TP"]):
        outs[t] = open("%s_%s%s" % (prefix, s, appendix), 'w')

    for c in enhancers:
        for e in enhancers[c]:
            outs[e.getType()].write(e.asBedStr() + "\n")
    for o in outs:
        outs[o].close()

def loadData(prefix, appendix, interFiles, isBedpes, clusterFiles, bedCols,
             tssFile, expCol, use_extension=False, thresh=0, geneNameMapFile=None, broad=False, enhancerFile=None):

    interactions = []
    for interFile, bedpe in zip(interFiles, isBedpes):
        interactions += INTERACTION.loadInteraction(interFile,
                                                    bedpe=bedpe)
    clusters = {}
    for clusterFile, bedCol in zip(clusterFiles, bedCols):
        CLUSTER.loadCAGEClusters(clusterFile, bedCol, clusters, thresh, expCol)

    id2NameMap = None
    if geneNameMapFile is not None:
        id2NameMap = TSS.makeNameMapDict(geneNameMapFile)
    tsses = TSS.loadTssFromBed(tssFile, id2NameMap)

    enhancers = {}

    if enhancerFile is not None:
        enhancers = ENHANCER.loadEnhancersFromROSETable(enhancerFile, by_chrom=True, tsses = tsses, tss_dist = 4000) if not broad \
            else ENHANCER.loadGappedPeaks(enhancerFile, by_chrom=True, tsses=tsses, tss_dist=4000)
        writeSepEnhancerBed(enhancers, prefix, appendix)
    else:
        enhancers = ENHANCER.loadEnhancersFromBedList(
            ["%s_%s%s" % (prefix, i, appendix) for i in ["SE", "SP", "TE", "TP"]],
            [TYPE_SE, TYPE_SP, TYPE_TE, TYPE_TP],
            use_extension)

    osp.make_counts(enhancers, interactions)
    CLUSTER.mapCAGEToRegions(tsses, clusters, dist_allowed=50, sameStrand=True, tssOnly=True)
    if enhancerFile is None:
        TSS.mapTssToEnhancer(enhancers, tsses, center_dist=4000)
    CLUSTER.mapCAGEToRegions(enhancers, clusters)

    return enhancers, interactions, clusters, tsses


def getEnhancerKey(e):
    key = 0
    if len(e.getPartners()) > 0:
        key = sum(list(set([p.getType() for p in e.getPartners()])))
    return key




def getCageTssDataByType(enhancers):
    spCageData = [[] for i in range(2**len(TYPES))]
    tpCageData = [[] for i in range(2**len(TYPES))]

    for c in enhancers:
        for e in enhancers[c]:
            currData = None
            if e.getType() == TYPE_SP:
                currData = spCageData
            elif e.getType() == TYPE_TP:
                currData = tpCageData
            if currData != None:
                tempKey = getEnhancerKey(e)
                currData[tempKey] += e.getTssClusters()
    return spCageData, tpCageData


def getEnhancerCageDataByType(enhancers, types=(TYPE_SE, TYPE_TE)):
    retData = [[[] for j in range(16)] for i in types]

    for c in enhancers:
        for e in enhancers[c]:
            currData = None
            if e.getType() in types:
                currData = retData[types.index(e.getType())]
            if currData != None:
                tempKey = getEnhancerKey(e)
                currData[tempKey] += e.getEnhancerClusters()

    return retData


def getCollatedGroupColumn(data, iGroups, column, expCol, thresh=0, useLog=False):
    result = []

    for ig in iGroups:
        temp = []
        for i in ig:
            for c in data[i]:
                if c.getExprs([expCol])[0] >= thresh:
                    if not useLog:
                        temp.append(c.data[column])
                    else:
                        temp.append(np.log10(c.data[column]))
        result.append(temp)

    return result


def savefig(fig, filePrefix):
    fig.tight_layout()
    fig.savefig(filePrefix + ".png", dpi=300)
    fig.savefig(filePrefix + ".pdf", dpi=600)
    fig.savefig(filePrefix + ".eps", dpi=600)



def getColors(types):
    return [INTRA_COLORS[i] for i in types]

def makeFigures(processedData, ylabel, figName, thresh, xlabel="Interaction partner", colors=getColors(TYPE_STRS),
                xtick_labels=TYPE_STRS +  ['None'], figwidth=col1Width, use_pairs=None, showMeans=False):
    fig, ax = pl.subplots(1, 1, figsize=(figwidth, col1Width))
    print(len(xtick_labels), len(processedData))
    sampleSizes = [np.sqrt(len(x) * 1.0) for x in processedData]
    boxWidths = [x / sum(sampleSizes) for x in sampleSizes]
    flierprops = dict(marker='+', color="#aaaaaa", markersize=2, alpha=0.25)
    num_spaces = [len(a) - len(str(len(b))) + 2 for a,b in zip(xtick_labels, processedData)]
    xtick_labels = ["%s\n(%d)%s" % (a,len(b)," "*c) for a,b,c in zip(xtick_labels, processedData, num_spaces)]
    if not showMeans:
        box = ax.boxplot(processedData, labels=xtick_labels, notch=False, flierprops=flierprops, widths=boxWidths,
                         patch_artist=True, showmeans=showMeans)
    else:
        box = ax.boxplot(processedData, labels=xtick_labels, notch=False, flierprops=flierprops, widths=boxWidths,
                         patch_artist=True, showmeans=showMeans,
                         meanprops=dict(marker='D', markeredgecolor='black', markerfacecolor='green', markersize=4))

    print(len(processedData), len(xtick_labels))
    if max(map(len, xtick_labels)) > 8:
        rotation = 25
    else:
        rotation = 0
    ax.set_xticklabels(xtick_labels, fontsize=tick_fontsize-1, rotation=rotation)
    ax.set_ylabel(ylabel, fontsize=axislabel_fontsize-1)
    if xlabel != None:
        ax.set_xlabel(xlabel, fontsize=axislabel_fontsize)

    # remove top and right frame
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.tick_params(axis='x', direction='out')

    for i in range(len(colors)):
        curr_color = colors[i]
        box['boxes'][i].set_facecolor(curr_color)
        box['boxes'][i].set_color(curr_color)

        box['whiskers'][i * 2].set_color(curr_color)
        box['whiskers'][i * 2 + 1].set_color(curr_color)

        box['whiskers'][i * 2].set_linewidth(1)
        box['whiskers'][i * 2 + 1].set_linewidth(1)

        box['medians'][i].set_color('black')
        box['medians'][i].set_linewidth(1.2)

        box['caps'][i * 2].set_color(curr_color)
        box['caps'][i * 2 + 1].set_color(curr_color)
    for c in box['caps']:
        c.set_linewidth(1)

    print("===counts for %s===" % figName)
    addStarToBoxplot(processedData, xtick_labels, ax, box, use_pairs)
    savefig(fig, "plots/%s_tpm%d" % (figName, thresh))
    pl.close()


def addStarToBoxplot(processedData, xtick_labels, ax, box, use_pairs):
    for x, p in zip(xtick_labels, processedData):
        print(x.replace("\n"," "), len(p))
    hstat, pval_all, h_groups, p_groups, diff_groups = dunn.kw_dunn(processedData)
    print(hstat, pval_all)

    print("ANOVA: ", f_oneway(*processedData))
    print("\n")

    to_compare = tuple(combinations(range(len(processedData)), 2))
    significant_pairs = []
    # significant_p_values = []

    max_y = np.nanmax([box['caps'][i * 2 + 1].get_ydata()[0] for i in range(len(processedData))])
    min_y = np.nanmin([box['caps'][i * 2].get_ydata()[0] for i in range(len(processedData))])

    if pval_all >= 0.05 and use_pairs is None:
        ax.set_ylim([ax.get_ylim()[0], max_y * 1.1])
        return

    for g, a, b, c in zip(to_compare, h_groups, p_groups, diff_groups):
        print(xtick_labels[g[0]].replace("\n", ' '), 'vs', xtick_labels[g[1]].replace("\n", " "), a, b, c)
        if (use_pairs is None and c) or (use_pairs and g in use_pairs):
            significant_pairs.append((g, b))

    significant_pairs.sort(key=lambda k: (k[0][1] - k[0][0], k[0][0]))
    levels = [1 for i in significant_pairs]

    def pair_overlap(pairA, pairB):
        return max(pairA[0], pairB[0]) < min(pairA[1], pairB[1])

    def star(curr_p):
        return "%.1E" % curr_p

    def color(curr_p):
        if curr_p < 0.05:
            return "red"
        return "green"

    for i in range(len(significant_pairs)):
        pair = significant_pairs[i][0]
        if use_pairs and pair not in use_pairs:
            continue
        p = significant_pairs[i][1]

        invalid_levels = set()
        for j in range(i):
            if pair_overlap(pair, significant_pairs[j][0]):
                invalid_levels.add(levels[j])
                if levels[i] == levels[j]:
                    levels[i] = levels[j] + 1
        while levels[i] in invalid_levels:
            levels[i] += 1
        ax.annotate("", xy=(pair[0] + 1.1, max_y + levels[i] * 0.1 * (max_y - min_y)), xycoords='data',
                    xytext=(pair[1] + 0.9, max_y + levels[i] * 0.1 * (max_y - min_y)), textcoords="data",
                    arrowprops=dict(arrowstyle="-", ec='#000000',
                                    connectionstyle="bar,fraction=%f" % (0.08 / (pair[1] - pair[0]))))
        ax.text(0.5 * (pair[0] + pair[1]) + 1, max_y + levels[i] * 0.1 * (max_y - min_y) + 0.06 * (max_y - min_y),
                star(p), horizontalalignment="center", verticalalignment="center", size=8, color=color(p))
    print(significant_pairs, levels)
    if len(levels) == 0:
        levels = [0]
    ax.set_ylim([ax.get_ylim()[0], max_y + max(levels) * 0.13 * (max_y - min_y)])


def makeCollatedPlot(data, column, iGroups, thresh, xlabel, ylabel,
                     figName, expCol, colors=getColors(TYPE_STRS), xtick_labels=TYPE_STRS+["None"],
                     figwidth=col1Width, use_pairs=None, useLog=False):
    collatedValues = []
    if column != 'fold':
        for d in data:
            collatedValues += getCollatedGroupColumn(d, iGroups, column, expCol, thresh=thresh, useLog=useLog)
    else:
        collatedValues = []
        for d in data:
            for ig in iGroups:
                temp = []
                for i in ig:
                    tempData = []
                    for cagec in d[i]:
                        if cagec.getExprs([expCol, ])[0] >= thresh:
                            tempData.append(cagec.getFoldChangeAgainstPercentile(expCol, 50))
                    temp += tempData
                collatedValues.append(temp)
    print(len(collatedValues), xtick_labels)
    makeFigures(collatedValues, ylabel, figName, thresh, xlabel=xlabel, colors=colors, xtick_labels=xtick_labels,
                figwidth=figwidth, use_pairs=use_pairs)




def getDistToTss(enhancers, tsses, activeTsses, distalTypes=(TYPE_SE, TYPE_TE), proximalTypes=(TYPE_SP, TYPE_TP)):
    dists = [[], [], []]
    diffs = [[], [], []]

    def getDistToCenter(c, t, log=False):
        if log:
            return np.log10(abs(c - t.start * 0.5 - t.end * 0.5))
        else:
            return abs(c - t.start * 0.5 - t.end * 0.5)

    for chrom in enhancers:
        for e in enhancers[chrom]:
            if e.getType() in distalTypes:
                center = (e.start + e.end) * 0.5

                tempDists = []
                hasSP = False
                countTsses = 0
                for p in e.getPartners():
                    if p.getType() in proximalTypes:
                        tempMin = 10000000000
                        hasSP = True
                        for t in p.getTsses():
                            countTsses += 1
                            if getDistToCenter(center, t, True) < tempMin:
                                tempMin = getDistToCenter(center, t, True)
                        if tempMin != 10000000000:
                            tempDists.append(tempMin)
                assert (len(tempDists) > 0 and hasSP) or (len(tempDists) <= 0 and not hasSP), "%d tempDists and hasSP is %s and tsses %d" % (len(tempDists), hasSP, countTsses)
                if len(tempDists) > 0:
                    dists[2] += tempDists
                if hasSP:
                    for t in TSS.findNearestTss(e.chrom, e.start, e.end, tsses):
                        dists[0].append(getDistToCenter(center, t, True))
                    for t in TSS.findNearestTss(e.chrom, e.start, e.end, activeTsses):
                        dists[1].append(getDistToCenter(center, t, True))
                    diffs[0].append(dists[1][-1] - dists[0][-1])
                    diffs[1] += [min([x - dists[0][-1] for x in tempDists]), ]
                    diffs[2] += [min([x - dists[1][-1] for x in tempDists]), ]

    return dists, diffs


def plotDistToTssDistri(dists, labels, figName):
    assert len(dists) == len(labels)
    weights = []
    for i in range(len(dists)):
        weights.append(np.ones_like(dists[i]) * 1.0 / len(dists[i]))
    ys, centers, _ = pl.hist(dists, bins=20, weights=weights)
    pl.clf()
    print(centers)
    centers = 0.5 * (centers[:-1] + centers[1:])
    fig, ax = pl.subplots(1, 1, figsize=(1.5 * col1Width, col1Width))
    idx = 0

    for y, l in zip(ys, labels):
        ax.plot(centers, y, label=l + " (%d)" % len(dists[idx]), linewidth=2.0, alpha=0.6)
        idx += 1

    ax.set_xlabel("Distance of TSS to Distal Enhancer (log10 (bp))", fontsize=axislabel_fontsize)
    ax.set_ylabel("Frequency", fontsize=axislabel_fontsize)
    ax.legend(loc='upper right', prop={'size': 10})



    fig.tight_layout()
    fig.savefig("plots/{}.png".format(figName), dpi=300)
    fig.savefig("plots/{}.pdf".format(figName), dpi=600)
    fig.savefig("plots/{}.eps".format(figName), dpi=600)

    print("===counts for %s===" % figName)
    for x, p in zip(labels, dists):
        print(x, len(p))
    hstat, pval_all, h_groups, p_groups, diff_groups = dunn.kw_dunn(dists)
    print(hstat, pval_all)
    to_compare = tuple(combinations(range(len(dists)), 2))
    for g, a, b, c in zip(to_compare, h_groups, p_groups, diff_groups):
        print(labels[g[0]], 'vs', labels[g[1]], a, b, c)
    print("\n")


def writeToTable(enhancers, outBasename, expCol, ogs=None, tsgs=None, ccgs=None, rnaseq_expression=None, rnaseq_gene_expression=None, is_bd=False):
    out = open(outBasename + "_sup_table.tsv", 'w')
    header = ["#chrom", "start", "end", "name", "score", "strand", "type",
              "spanned_genes", "spanned_CAGE_exprs", "spanned_RNASEQ_exprs", "number_interactions"] + [
                 "number_interacting_{}".format(t) for t in TYPE_STRS] + [
                 "partner_%s_genes" % TYPE_STRS[1], "partner_%s_CAGE_exprs" % TYPE_STRS[1], "partner_%s_RNASEQ_exprs" % TYPE_STRS[1],
                 "partner_%s_genes" % TYPE_STRS[3], "partner_%s_CAGE_exprs" % TYPE_STRS[3], "partner_%s_RNASEQ_exprs" % TYPE_STRS[3], "max_cage_score"]
    out.write("\t".join(header) + "\n")

    tsg_i = 0
    og_i = 1
    ccg_i = 2
    all_i = 3
    anno_str = {"<TSG>": 0, "<OG>": 1, "<CCG>": 2}
    if not is_bd:
        reg_types = ["pse_p", "pse_d", "dse_d", "pte_p", "pte_d", "dte_d"]
    else:
        reg_types = ["pbd_p", "pbd_d", "dbd_d", "ptd_p", "ptd_d", "dtd_d"]
    gene_strs = ["tsg", "og", "ccg", "all"]
    clusters = {}
    rnaseqs = {}
    for r in reg_types:
        clusters[r] = []
        rnaseqs[r] = []
        for i in range(len(anno_str.keys())):
            clusters[r].append([])
            rnaseqs[r].append([])
        clusters[r].append([])
        rnaseqs[r].append([])

    for c in enhancers:
        for e in enhancers[c]:
            base_e_str = TYPE_STRS[TYPES.index(e.getType())].lower()
            line = []
            line.append(e.asBedStr())
            line.append(e.getTypeStr().replace(' ', '_'))

            spanTsses = "::".join([t.getNameStr(ogs=ogs, tsgs=tsgs, ccgs=ccgs) for t in e.getTsses()])
            line.append(spanTsses)
            max_cage_scores = []
            if e.getType() == TYPE_SP or e.getType() == TYPE_TP:
                e_str = base_e_str + "_p"
                for t in e.getTsses():
                    max_cage_scores.append(t.get_max_cage_score(expCol, 10))
                    curr_name = t.getNameStr(ogs=ogs, tsgs=tsgs, ccgs=ccgs)
                    for ann_t in anno_str:
                        if ann_t in curr_name:
                            clusters[e_str][anno_str[ann_t]] += filter(lambda x: x.getExprs([expCol])[0] >= 10, t.clusters)
                    clusters[e_str][-1] += filter(lambda x: x.getExprs([expCol])[0] >= 10, t.clusters)
                    temp_og, temp_tsg, _, temp_ccg, temp_all = t.get_rnaseq_gene_expression_for_og_tsg(rnaseq_gene_expression, ogs=ogs, tsgs=tsgs, ccgs=ccgs)
                    rnaseqs[e_str][tsg_i] += temp_tsg
                    rnaseqs[e_str][og_i] += temp_og
                    rnaseqs[e_str][ccg_i] += temp_ccg
                    rnaseqs[e_str][all_i] += temp_all
            else:
                for temp_c in e.getEnhancerClusters():
                    if temp_c.isEnhancer() and temp_c.getExprs([expCol])[0] >= 1:
                        max_cage_scores.append(temp_c.score)

            line.append("::".join(map(str, [t.get_expression(expCol) for t in e.getTsses()])))
            line.append(
                "::".join([";".join(map(str, t.get_rnaseq_expression(rnaseq_expression))) for t in e.getTsses()]))

            line.append(len(e.getInteractions()[0]))
            line += [len(e.getPartners(e_type=t)) for t in TYPES]
            partnerPSEgenes = []
            partnerPSEexprs = []
            partnerPSErnaseq = []
            partnerPTEgenes = []
            partnerPTEexprs = []
            partnerPTErnaseq = []
            for p in e.getPartners():
                tempPgenes = ''
                if p.getType() == TYPE_SP or p.getType() == TYPE_TP:
                    tempPgenes = "::".join([t.getNameStr(ogs=ogs, tsgs=tsgs, ccgs=ccgs) for t in p.getTsses()])
                    tempPexprs = "::".join(map(str, [t.get_expression(expCol) for t in p.getTsses()]))
                    tempPrnaseq = "::".join([";".join(map(str, t.get_rnaseq_expression(rnaseq_expression)))
                                             for t in p.getTsses()])
                if tempPgenes != "":
                    if p.getType() == TYPE_SP:
                        partnerPSEgenes.append(tempPgenes)
                        partnerPSEexprs.append(tempPexprs)
                        partnerPSErnaseq.append(tempPrnaseq)
                    else:
                        partnerPTEgenes.append(tempPgenes)
                        partnerPTEexprs.append(tempPexprs)
                        partnerPTErnaseq.append(tempPrnaseq)
                e_str = base_e_str + "_d"
                for t in p.getTsses():
                    curr_name = t.getNameStr(ogs=ogs, tsgs=tsgs, ccgs=ccgs)
                    for ann_t in anno_str:
                        if ann_t in curr_name:
                            clusters[e_str][anno_str[ann_t]] += filter(lambda x: x.getExprs([expCol])[0] >= 10, t.clusters)
                    clusters[e_str][-1] += filter(lambda x: x.getExprs([expCol])[0] >= 10, t.clusters)
                    temp_og, temp_tsg, _, temp_ccg, temp_all = t.get_rnaseq_gene_expression_for_og_tsg(rnaseq_gene_expression, ogs=ogs, tsgs=tsgs, ccgs=ccgs)
                    rnaseqs[e_str][tsg_i] += temp_tsg
                    rnaseqs[e_str][og_i] += temp_og
                    rnaseqs[e_str][ccg_i] += temp_ccg
                    rnaseqs[e_str][all_i] += temp_all
            line.append("&&".join(partnerPSEgenes))
            line.append("&&".join(partnerPSEexprs))
            line.append("&&".join(partnerPSErnaseq))
            line.append("&&".join(partnerPTEgenes))
            line.append("&&".join(partnerPTEexprs))
            line.append("&&".join(partnerPTErnaseq))
            line.append("::".join(map(str, max_cage_scores)))

            for i in range(len(line)):
                if line[i] == '':
                    line[i] = '.'
            out.write("\t".join(map(str, line)) + "\n")

    out.close()
    for k in clusters:
        for ki in range(len(clusters[k])):
            clusters[k][ki] = list(set(clusters[k][ki]))
            rnaseqs[k][ki] = list(set(rnaseqs[k][ki]))

    for k in clusters:
        print(k, " ".join(map(str, [len(ki) for ki in clusters[k]])))

    gene_types_to_use = []
    gene_strs_to_use = []
    if tsgs is not None:
        gene_types_to_use.append(0)
        gene_strs_to_use.append(gene_strs[gene_types_to_use[-1]])
    if ogs is not None:
        gene_types_to_use.append(1)
        gene_strs_to_use.append(gene_strs[gene_types_to_use[-1]])
    if ccgs is not None:
        gene_types_to_use.append(2)
        gene_strs_to_use.append(gene_strs[gene_types_to_use[-1]])
    gene_types_to_use.append(3)
    gene_strs_to_use.append(gene_strs[gene_types_to_use[-1]])


    all_clusters = []
    all_rnaseqs = []
    plot_range = 3
    for k in reg_types[:plot_range]:
        for ki in gene_types_to_use:
            all_clusters.append(clusters[k][ki])
            all_rnaseqs.append(rnaseqs[k][ki])
    makeFigures([[np.log10(c.getExprs([expCol])[0]) for c in t] for t in all_clusters],
                "CAGE expression (log10(TPM))",
                 outBasename + "_tsg_og_exprs", 0, xlabel="", colors=GENE_COLORS*plot_range,
                 xtick_labels=["%s_%s" % x for x in product(reg_types[:plot_range], gene_strs_to_use)],
                figwidth=col2Width, use_pairs=[], showMeans=True)
    for k in reg_types:
        for ki in range(len(rnaseqs[k])):
            print(k, gene_strs[ki], len(rnaseqs[k][ki]), " ".join(sorted([c[0] for c in rnaseqs[k][ki]])))
        print("==================================================\n")

    makeFigures([[np.log10(c[1]) for c in t] for t in all_rnaseqs],
                "RNA-seq expression (log10(FPKM))",
                outBasename + "_tsg_og_rnaseq_exprs", 0, xlabel="", colors=GENE_COLORS*plot_range,
                xtick_labels=["%s_%s" % x for x in product(reg_types[:plot_range], gene_strs_to_use)],
                figwidth=col2Width, use_pairs=[], showMeans=True)


def plotFracInter(enhancers, figName, figWidth=col1Width):
    numInterByType = [[] for t in TYPES]
    totalCounts = [0 for t in TYPES]
    for c in enhancers:
        for e in enhancers[c]:
            numInterByType[TYPES.index(e.getType())].append(len(e.getInteractions()[0]))
            totalCounts[TYPES.index(e.getType())] += 1
    fig, ax = pl.subplots(1, 1, figsize=(figWidth, col1Width))
    inter_fractions = [len(filter(lambda k: k > 0, numInterByType[i])) * 1.0 / totalCounts[i] for i in
                       range(len(TYPES))]
    rects = osp.ppl.bar(ax, np.arange(1, len(inter_fractions) + 1) - 0.4, inter_fractions, width=0.8,
                        color=getColors(TYPE_STRS))
    ax.set_xlim([0.2, len(inter_fractions) + 0.8])
    ax.set_xticks(np.arange(1, len(inter_fractions) + 1))
    ax.set_xticklabels(TYPE_STRS, fontsize=tick_fontsize, rotation=25)
    osp.autolabel(rects, ax, fontsize=tick_fontsize)
    ax.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    ax.set_ylabel("Fraction with interaction", fontsize=axislabel_fontsize)
    fig.tight_layout()
    fig.savefig("plots/" + figName + "_Inter_frac.png", dpi=300)
    fig.savefig("plots/" + figName + "_Inter_frac.pdf", dpi=600)
    fig.savefig("plots/" + figName + "_Inter_frac.eps", dpi=600)


    makeFigures([filter(lambda k: k > 0, numInterByType[i]) for i in range(len(TYPES))], "Number of Interactions",
                figName + "_num_inter_boxplot", 0, xlabel="", colors=getColors(TYPE_STRS), xtick_labels=TYPE_STRS,
                figwidth=figWidth, use_pairs=None, showMeans=True)





def getColorRgb():

    def hex_to_rgb(value):
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

    for c in INTRA_COLORS:
        print(",".join(map(str, hex_to_rgb(c))))


def load_rnaseq_levels(files, name_col, expr_col):
    expression_levels = {}
    for file_to_use in files:
        f = open(file_to_use)
        f.readline()
        for r in f:
            tokens = r.strip().split()
            names = tokens[name_col].strip().strip(',').split(',')
            for n in names:
                if n not in expression_levels:
                    expression_levels[n] = 0
                expression_levels[n] += float(tokens[expr_col])
        f.close()

    for k in expression_levels:
        expression_levels[k] /= len(files)

    return expression_levels


def main():
    import argparse
    parser = argparse.ArgumentParser("The combined script for generating the data.")
    parser.add_argument("data", type=str, choices=["k562", "k562_h3k4me3", "mcf7", "12kb", "mcf7_h3k4me3"],
                        help="The configuration to use. [k562, k562_h3k4me3, 12kb, mcf7, mcf7_h3k4me3]")
    parser.add_argument("-e", "--extend", action="store_true", default=False,
                        help="Whether to extend.")
    parser.add_argument("-t", "--tissue", default="cells", choices=["cells", "tissues", "celllines"],
                        help="the tissue type for cage data. default: cells")
    parser.add_argument("--ogfile", default=None,
                        help="The file that contains the list of oncogenes. One gene per line.")
    parser.add_argument("--tsgfile", default=None,
                        help="The file that contains the list of tumour suppressors. One gene per line.")
    parser.add_argument("--ccgfile", default=None,
                        help="The file that contains the list of census cancer genes from COSMIC. One gene per line.")
    parser.add_argument("--interFile", default=None,
                        help="Use custom interaction file instead of the hard coded files. The file should be in bedpe format.")
    parser.add_argument("--workingDir", required=True,
                        help="The workint directory. Required")
    args = parser.parse_args()


    fd = os.path.dirname(os.path.realpath(__file__)) + "/data/"
    global TYPE_STRS
    global TYPE_NAMES


    os.chdir(args.workingDir)
    if not os.path.isdir(os.path.join(args.workingDir, "plots")):
        os.mkdir(os.path.join(args.workingDir, "plots"))


    tssFile = fd + "gencode.v19.annotation_capped_sites_nr_with_confidence.bed"
    geneNameMapFile = fd + "gencode.v19.transcript_genename_hgnc_map.txt"

    if args.data in ["k562", "k562_h3k4me3", "12kb"]:
        expCol = "exprlek562"
        rnaseq_expression_files = [fd + "rnaseq/" + i for i in [
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep1_Techrep1_ENCFF293IVM.tsv",
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep1_Techrep1_ENCFF363JRF.tsv",
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep2_Techrep1_ENCFF187BSB.tsv",
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_transcript_quantifications_Biorep2_Techrep1_ENCFF976JSC.tsv"
        ]]

        rnaseq_gene_expression_files = [fd + "rnaseq/" + i for i in [
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_gene_quantifications_Biorep1_Techrep1_ENCFF139IXQ.tsv",
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_gene_quantifications_Biorep1_Techrep1_ENCFF534SDC.tsv",
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_gene_quantifications_Biorep2_Techrep1_ENCFF088RDE.tsv",
            "ENCODE_ENCODE_Processing_Pipeline_RNA-seq_K562_gene_quantifications_Biorep2_Techrep1_ENCFF450IVD.tsv"
        ]]
        activeTssFile = fd + "gencode.v19.annotation_capped_sites_nr_with_confidence.ext50.merged.k562_tpm10.bed"
        interFiles = [fd + "k562_rep0.merged.intra.bedpe"]

    elif args.data in ["mcf7", "mcf7_h3k4me3"]:
        expCol = "exprleMCF7"
        rnaseq_expression_files = [fd + "rnaseq/" + i for i in [
            'ENCFF615MUV.tsv',
            'ENCFF703QWZ.tsv']]

        rnaseq_gene_expression_files = [fd + "rnaseq/" + i for i in [
            'ENCFF151JZX.tsv',
            'ENCFF061AQT.tsv']]
        activeTssFile = fd + "gencode.v19.annotation_capped_sites_nr_with_confidence.ext50.merged.mcf7_tpm10.bed"
        interFiles = [fd + "mcf7_rep0.merged.intra.bedpe"]



    enhancerFile = None
    is_bd = False

    if "h3k4me3" in args.data:
        is_bd = True
        TYPE_STRS = ["DBD", "PBD", "DTD", "PTD"]
        TYPE_NAMES = ["Distal BD", "Proximal BD", "Distal TD", "Proximal TD"]


    if args.data == "k562":
        enhancerDir = fd + "ses/"
        prefix = "revision_k562_h3k27ac_4k"
        size = "4kb"
        enhancerFile = fd + "ses/revision_k562_h3k27ac_AllEnhancers.table.txt"
    elif args.data == "k562_h3k4me3":
        enhancerDir = fd + "ses/"
        enhancerFile = fd + "ses/k562_h3k4me3_optimal_broad_peaks.gappedPeak"
        prefix = "revision_k562_h3k4me3"
        size = "XX"
    elif args.data == "12kb":
        enhancerDir = fd + "ses/"
        enhancerFile = fd + "ses/revision_k562_h3k27ac_12kb_AllEnhancers.table.txt"
        prefix = "revision_k562_h3k27ac_12k"
        size = "12kb"
    elif args.data == "mcf7":
        enhancerDir = fd + "ses/"
        prefix = "revision_mcf7_h3k27ac_4k"
        enhancerFile = fd + "ses/revision_mcf7_h3k27ac_AllEnhancers.table.txt"
        size = "4kb"
    elif args.data == "mcf7_h3k4me3":
        enhancerDir = fd + "ses/"
        enhancerFile = fd + "ses/mcf7_h3k4me3_optimal_broad_peaks.gappedPeak"
        prefix = "revision_mcf7_h3k4me3"
        size = "XX"






    tissueType = args.tissue

    rnaseq_expressions = load_rnaseq_levels(rnaseq_expression_files, 0, 6)
    rnaseq_gene_expressions = load_rnaseq_levels(rnaseq_gene_expression_files, 1, 6)

    outPrefix = "{}_{}_{}".format(args.data, tissueType, size)
    appendix = ".bed"
    use_extension = args.extend
    if use_extension:
        enhancerFile = None
        outPrefix = outPrefix + "_extended"

    if args.interFile is not None:
        interFiles = [args.interFile,]
        outPrefix = outPrefix + "_custom_interFile"
    isBedPes = [True for f in interFiles]

    clusterFiles = ["{}robust_facets_{}{}.osc"
                        .format(fd, tissueType, x) for x in ["", "_enhancers"]]
    bedCols = [6, 13]

    thresh = 10
    # loading data
    enhancers, interactions, clusters, tsses = loadData(enhancerDir + prefix, appendix,
                                                        interFiles, isBedPes,
                                                        clusterFiles, bedCols,
                                                        tssFile, expCol,
                                                        use_extension=use_extension,
                                                        geneNameMapFile=geneNameMapFile,
                                                        enhancerFile = enhancerFile,
                                                        broad = is_bd)
    spCageData, tpCageData = getCageTssDataByType(enhancers)
    enhancerCageData = getEnhancerCageDataByType(enhancers, [TYPE_SE, TYPE_SP, TYPE_TE, TYPE_TP])
    

    for i in range(16):
        print(len(spCageData[i]))

    allData = [[spCageData[i] + tpCageData[i] for i in range(16)]]
    columns = ["score", expCol, ]

    dataNames = ["SP_TP"]
    suffixes = ["Specificity", "Expr", "Fcs"]
    ylabels = ["Specificity score", "Expression (TPM)", "Fold change (log2)"]
    indexGroups = [[i for i in range(16) if i & 8 == 8], [i for i in range(16) if i & 4 == 4],
                   [i for i in range(16) if i & 2 == 2], [i for i in range(16) if i & 1 == 1],
                   [0], ]

    xtick_labels = TYPE_STRS
    for i in range(len(allData)):
        for j in range(len(columns)):
            makeCollatedPlot([allData[i], ], columns[j], indexGroups, thresh,
                             "Interaction partner type", ylabels[j],
                             "{}_Collated_{}_{}".format(outPrefix, dataNames[i], suffixes[j]),
                             expCol, xtick_labels=xtick_labels + ["None"],
                             colors = getColors(TYPE_STRS),
                             use_pairs=None,
                             figwidth=col1Width)

    pl.clf()

    groups = [range(1, 16), [0, ]]
    for j in range(len(columns)):
        makeCollatedPlot(allData, columns[j], groups, thresh, "", ylabels[j],
                         "{}_Inter_vs_NoInter_{}_{}".format(outPrefix, "combined", suffixes[j]), expCol,
                         colors=GENE_COLORS[:2] * len(allData),
                         xtick_labels=["%s with Inter" % TYPE_STRS[1], "%s w/o Inter" % TYPE_STRS[1],
                                       "%s with Inter" % TYPE_STRS[3], "%s w/o Inter" % TYPE_STRS[3],
                                       "All with Inter", "All w/o Inter"],
                         figwidth=col2Width, use_pairs=[(0, 1), (2, 3), (4, 5)])

    
    plotFracInter(enhancers, outPrefix)
    pl.clf()
    for j in range(len(columns)):
        makeCollatedPlot([spCageData, tpCageData], columns[j], [range(16), ], thresh, "", ylabels[j],
                         "{}_PSE_vs_PTE_{}".format(outPrefix, suffixes[j]), expCol, colors=getColors([TYPE_STRS[jk] for jk in [1,3]]),
                         xtick_labels=[TYPE_STRS[1], TYPE_STRS[3]],
                         figwidth=col1Width)

    for j in range(len(columns)):
        makeCollatedPlot([enhancerCageData[k] for k in [0,2]], columns[j], [range(16), ], 1,
                         "", ylabels[j],
                         "{}_ERNA_{}".format(outPrefix, suffixes[j]), expCol, colors=getColors([TYPE_STRS[jk] for jk in [0,2]]),
                         xtick_labels=[TYPE_STRS[k] for k in [0,2]])

    for j in range(len(columns)):
        makeCollatedPlot([enhancerCageData[k] for k in [0, 2]], columns[j], [range(1, 16), [0, ]], 1,
                         "", ylabels[j],
                         "{}_ERNA_Inter_vs_NoInter_{}".format(outPrefix, suffixes[j]), expCol, colors=GENE_COLORS[:2] * 2,
                         xtick_labels=[' '.join(i) for i in product([TYPE_STRS[0], TYPE_STRS[2]], ['with Inter', 'w/o Inter'])],
                         use_pairs=[(0, 1), (2, 3)])

    


    

    tssesActive = TSS.loadTssFromBed(activeTssFile)

    
    dists, diffs = getDistToTss(enhancers, tsses, tssesActive)
    labels = ["Nearest", "Nearest active", "By ChIA-PET"]
    plotDistToTssDistri(dists, labels, "{}_Tss_distance_to_DSE".format(outPrefix))
    for t in dists:
        print(len(t))

    ogs = loadGenes(args.ogfile)
    tsgs = loadGenes(args.tsgfile)
    ccgs = loadGenes(args.ccgfile)
    writeToTable(enhancers, outPrefix, expCol, ogs, tsgs, ccgs, rnaseq_expressions, rnaseq_gene_expressions, is_bd = is_bd)


def loadGenes(filename):
    if filename is None:
        return None
    genes = set()
    f = open(filename)
    for r in f:
        genes.add(r.strip())
    f.close()
    return genes


if __name__ == '__main__':
    main()
